const API_URL = 'http://10.86.153.113:4000/api';

const userData = {
   _id: '',
   username: '',
   fullname: '',
   dob: '',
   email: '',
   phone: '',
   createDate: '',
   token: '',
};

export { API_URL, userData };
