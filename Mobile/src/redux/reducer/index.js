import { combineReducers } from 'redux';
import loginReducer from './account/loginReducer';
import signupReducer from './account/signupReducer';
import createWalletReducer from './home/createWalletReducer';
import getWalletReducer from './home/getWalletReducer';
import deleteWalletReducer from './home/deleteWalletReducer';
import updateWalletReducer from './home/updateWalletReducer';
import sendOTPReducer from './account/forget/sendOTPReducer'
import verifyOTPReducer from './account/forget/verifyOTPReducer';
import createNewPassReducer from './account/forget/createNewPassReducer';

const allReducers = combineReducers({
   loginReducer,
   signupReducer,
   createWalletReducer,
   getWalletReducer,
   deleteWalletReducer,
   updateWalletReducer,
   sendOTPReducer,
   verifyOTPReducer,
   createNewPassReducer,
});

export default allReducers;
